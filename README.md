This is `mtime` - a model time management library. It aims to provide time,
calendar, and event handling for model applications.

The library is [`REUSE`](https://reuse.software)-compliant and published under
the BSD 3-clause license (see the list of [authors](/AUTHORS.md)).

You can find more information in the
[documentation](https://icon-libraries.gitlab-pages.dkrz.de/libmtime) and
[contribution guidelines](/CONTRIBUTING.md).
