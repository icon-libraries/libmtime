- [Jan Frederik Engels](mailto:engels+git@dkrz.de)
- [Thomas Jahns](mailto:jahns@dkrz.de)
- [Luis Kornblueh](mailto:luis.kornblueh@mpimet.mpg.de)
- [Sergey Kosukhin](mailto:sergey.kosukhin@mpimet.mpg.de)
- [Ralf Mueller](mailto:ralf.mueller@dkrz.de)
- [Florian Prill](mailto:florian.prill@dwd.de)
- [Rahul Sinha](mailto:rahul.sinha@mpimet.mpg.de)
- [Michael Weimer](mailto:michael.weimer@kit.edu)
- [Karl-Hermann Wieners](mailto:karl-hermann.wieners@mpimet.mpg.de)
- [Christian Zoller](mailto:zoller@informatik.uni-hamburg.de)
- [Yen-Chen Chen](mailto:yen-chen.chen@kit.edu)
- [Moritz Hanke](mailto:hanke@dkrz.de)
